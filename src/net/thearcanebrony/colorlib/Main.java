package net.thearcanebrony.colorlib;

import net.thearcanebrony.colorlib.tests.Matrix;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Main extends JavaPlugin implements Listener {
    {
        plugin = this;
        System.out.println(Bukkit.getVersion());
        System.out.println(Bukkit.getBukkitVersion());
    }

    public static Boolean IsRunning = true;
    public static Plugin plugin;
    public static Plugin getPlugin() {
        return plugin;
    }

    @Override
    public void onEnable() {
        Util.logDebug("Debug output enabled! This may cause severe spam!");
    }

    //cleanup
    @Override
    public void onDisable() {
        IsRunning = false;
    }

    //commands
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player p = Bukkit.getPlayer(sender.getName());
        if (command.getName().equalsIgnoreCase("rgbtest")) {
            if (sender.hasPermission("spigotav.scan")) {
                new BukkitRunnable(){
                    int i = 1;
                    @Override
                    public void run() {
                        String output = "";
                        for (int x = 0; x < 128; x++) {
                            int r = (i),
                                    g = ((x+i)%127),
                                    b = (Math.abs(x-i)%127);
                            output += new RGBColor(r,g,b)+"M";
//                            output += ChatColor.of("#"+String.format("%02X",r)+String.format("%02X",g)+String.format("%02X",b))+"M";
                        }
                        if(sender instanceof Player) sender.sendMessage(output);
                        else ConUtil.println(output);
                        i++;
                        if(i<0) i=1;

                    }
                }.runTaskTimerAsynchronously(this, 0L, 1L);
            } else p.sendMessage(Config.ChatPrefix + "You do not have permission to use this command");
            return true;
        } else if (command.getName().equalsIgnoreCase("colortest")) {
            if (sender.hasPermission("colorlib.tests")) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        String output = "";
                        for (int r = 0; r < 255; r+=8) {
//                            System.out.println("a"+r);
                            for (int g = 0; g < 255; g+=8) {
//                                System.out.println("b"+g);
                                for (int b = 0; b < 255; b+=8) {
                                    //System.out.println("c"+b);
                                    output += new RGBColor(r,g,b)+"M";
                                }
                                try {
                                    Thread.sleep(00);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
//                                sender.sendMessage(output);
                                ConUtil.println(output);
                                output = "";
                            }
                        }
                    }
                }.runTaskAsynchronously(this);
            } else p.sendMessage(Config.ChatPrefix + "You do not have permission to use this command");
            return true;
        }
        else if (command.getName().equalsIgnoreCase("matrix")) {
            if (sender.hasPermission("colorlib.tests")) {
                ConUtil.println(Matrix.GetMatrix(80,30));
            } else p.sendMessage(Config.ChatPrefix + "You do not have permission to use this command");
            return true;
        }
        return false;
    }

}
