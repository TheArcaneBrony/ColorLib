package net.thearcanebrony.colorlib;

import net.thearcanebrony.colorlib.tests.ColorGrid;
import net.thearcanebrony.colorlib.tests.Matrix;
import net.thearcanebrony.colorlib.tests.RgbTest;
import org.fusesource.jansi.AnsiConsole;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

public class CLI {
    public static void main(String[] _args)
    {
        AnsiConsole.systemInstall();
        String baseDir = ".";
        List<String> args = Arrays.asList(_args);
        int arg;
        if(args.contains("--help")){
            System.out.println("SpigotAV help\n" +
                    "Supported arguments:\n" +
                    " --help\t\tShows this message\n" +
                    " --html\t\tExport results as HTML\n" +
                    " --verbose\tIncrease verbosity\n" +
                    " --cd\t\tChange current directory\n" +
                    " --launch\tLaunches a server, next argument: jar path (will be executed in current dir!)\n" +
                    " --dir\t\tDirectory to scan (defaults to current directory)\n" +
                    "\n" +
                    "Debug arguments:\n" +
                    " --rgbtest\tGive me them colors!! (test RGB output)");
            return;
        }
        if(args.contains("--verbose")){
            ConUtil.println("Option: Verbose");
            BaseConfig.Verbose = true;
        }
        if(args.contains("--rgbtest")){
            if(BaseConfig.Verbose) ConUtil.println("Generating " + new RGBColor(255,0,0) + "R"+ new RGBColor(0,255,0) + "G"+ new RGBColor(0,0,255) + "B"+ new RGBColor(255,255,255) + " string");
            ConUtil.println(new RgbTest().getRgbString());
            return;
        }
        if(args.contains("--matrix")){
            if(BaseConfig.Verbose) ConUtil.println("Generating matrix 80x30");
            ConUtil.println(Matrix.GetMatrix(210,50));
            return;
        }
        if(args.contains("--colorgrid")){
            if(BaseConfig.Verbose) ConUtil.println("Generating color grid 210x50");
            ConUtil.println(ColorGrid.GetColorGrid(210, 50));
            return;
        }
        if(args.contains("--colorgridl")){
            if(BaseConfig.Verbose) ConUtil.println("Generating color grid 210x50");
            ConUtil.println(ColorGrid.GetColorGrid(AnsiConsole.getTerminalWidth(), 150));
            return;
        }
        if((arg = args.indexOf("--cd")) >= 0){
            String newpath = args.get(arg+1);
            if(BaseConfig.Verbose) ConUtil.println("Option: Change current directory: "+ newpath);
            if(Files.exists(Path.of(newpath))){
                System.setProperty("user.dir", Path.of(newpath).toAbsolutePath().toString());
            }
            else {
                System.out.println("Invalid argument --cd: Directory does not exist");
                return;
            }
        }
        if((arg = args.indexOf("--dir")) >= 0){
            if(Files.exists(Path.of(args.get(arg+1)))){
                baseDir = args.get(arg+1);
            }
            else {
                System.out.println("Invalid argument --dir: Directory does not exist");
                return;
            }
        }
    }
}
