package net.thearcanebrony.colorlib;

import org.fusesource.jansi.AnsiConsole;

public class ConUtil {
    public static void install(){
        AnsiConsole.systemInstall();
    }
    public static void println(String text){
        print(text+"\n");
    }
    public static void print(String text){
        System.out.print(translateColors(text) + new RGBColor("FFFFFF").getAnsiColor());
    }
    private static String translateColors(String text){
        return RGBColor.ColorPattern.matcher(text).replaceAll(matchResult -> new RGBColor(matchResult.group(0)).getAnsiColor());
    }
}
