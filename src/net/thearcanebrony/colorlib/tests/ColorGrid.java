package net.thearcanebrony.colorlib.tests;

import net.thearcanebrony.colorlib.RGBColor;

import static java.lang.Math.sin;

public class ColorGrid {
    public static String GetColorGrid(int w, int h){
        Boolean neg = false;
        int gv = 0;
            String grid = "";
            for (int y = 0; y < h; y++) {
                for (int x = 0; x < w; x++) {
                    grid += new RGBColor(x * 255 / w, (int) ((sin((x+y)/16d)+1)*127), y * 255 / h).getAnsiColor() + "M";
                }
                grid += "\n";
//                ConUtil.println(grid);
//                grid = "";
            }
        return grid;
    }
}
