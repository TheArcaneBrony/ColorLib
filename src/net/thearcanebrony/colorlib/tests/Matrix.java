package net.thearcanebrony.colorlib.tests;

import net.thearcanebrony.colorlib.RGBColor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Matrix {
    public static String GetMatrix(int w, int h){
        List<Integer> py = new ArrayList();
        Random rnd = new Random();
        for (int i = 0; i < w; i++) {
            py.add(rnd.nextInt(h));
        }
        String mtrx = "";
        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                int cy = py.get(x);
                if(y>cy) mtrx += " ";
                else {
                    int brightness = 255-Math.min((int) (((cy-y)*2)/(double)h*255), 255);
                    mtrx += new RGBColor(0, brightness, x*brightness/w).toString() + rnd.nextInt(2);
                }
            }
            mtrx += "\n";
        }
        return mtrx;
    }
}
