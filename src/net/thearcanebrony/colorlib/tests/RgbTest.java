package net.thearcanebrony.colorlib.tests;

import net.thearcanebrony.colorlib.BaseConfig;
import net.thearcanebrony.colorlib.ConUtil;
import net.thearcanebrony.colorlib.RGBColor;

public class RgbTest {
    public String rgbString = "";
    public String getRgbString(){
        if(rgbString == ""){
            StringBuilder _rgb = new StringBuilder();
            StringBuilder _line = new StringBuilder();
            int i = 0;
            for (int r = 0; r <= 255; r+=8) {
                if(BaseConfig.Verbose) ConUtil.println(new RGBColor(r,0,0) + String.format("%2X%2X%2X %s", r,0,0,_rgb.length()));
                for (int g = 0; g <= 255; g+=8) {
                    for (int b = 0; b <= 255; b+=8) {
                        _line.append(new RGBColor(r, g, b)).append("M");
                        if(++i>=127) {
                            _rgb.append(_line+"\n");
                            _line.setLength(0);
                            i=0;
                        }
                    }
                }
            }
            rgbString = _rgb.toString();
        }
        return rgbString;
    }
}
