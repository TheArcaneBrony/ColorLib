package net.thearcanebrony.colorlib;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

import java.util.Arrays;

public class MCUtil {
    private static final String bukkitversion = Bukkit.getServer().getClass().getPackage().getName().substring(23);


    public static Plugin getPlugin() { return Main.plugin; }

    public static Server getServer() {
        return getPlugin().getServer();
    }

    public static FileConfiguration getConfig() {
        return getPlugin().getConfig();
    }

    public final static boolean ServerSupportsRGB = Arrays.stream(ChatColor.class.getDeclaredMethods()).anyMatch(x->x.getName().equals("of"));

    public static String GetGameColors(String str){
        if(ServerSupportsRGB){
            return "";
        }
        else {
            return "";
        }
    }
}
