#ColorLib
ColorLib is a project to allow plugin developers to use 24bit RGB color in their chat messages, whilst keeping backwards compatibility with older Minecraft servers.<br>
This project was made because I wanted to make another plugin backwards compatible with 1.12.2 (see SpigotAV: [Website](https://spigotav.thearcanebrony.net), [GitLab](https://gitlab.com/TheArcaneBrony/SpigotAV), Yatopia Discord).<br>
This project was made possible with the help of the people at the Spigot Discord!